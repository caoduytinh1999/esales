package com.hqsoft.esales.trainee;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.hqsoft.esales.trainee.adapter.CustomerAdapter;
import com.hqsoft.esales.trainee.model.Customer;
import com.hqsoft.esales.trainee.model.DatabaseTest;
import com.hqsoft.esales.trainee.my_interface.IOnClickItemListener;
import com.hqsoft.esales.trainee.my_interface.OnClick;

import java.util.ArrayList;
import java.util.List;

public class CustomerActivity extends AppCompatActivity {

    EditText editTextSearch;
    RecyclerView recyclerView;
    CustomerAdapter adapter;
    List<Customer> customerList = new ArrayList<>();
    List<Customer> customerListFilter = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);
        mapped();
        event();
    }

    private void event() {
        getData();

        LinearLayoutManager manager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        adapter = new CustomerAdapter(customerListFilter, new IOnClickItemListener() {
            @Override
            public void OnClick(OnClick onClick) {
                Customer customer = (Customer) onClick;
                String id = customer.getCustID();
                Intent intent = new Intent(CustomerActivity.this, OrderListActivity.class);
                intent.putExtra("CustID",id);
                startActivity(intent);
            }
        });
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);

        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                customerListFilter.clear();
                String filter = String.valueOf(s);
                if (filter.compareTo("") != 0){
                    for (Customer item : customerList){
                        if (item.getCustID().contains(filter) || item.getName().contains(filter) || item.getAddress().contains(filter) || item.getPhone().contains(filter)){
                            customerListFilter.add(item);
                        }
                    }
                }
                else{
                    customerListFilter.addAll(customerList);
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void mapped() {
        editTextSearch = findViewById(R.id.editTextSearch);
        recyclerView   = findViewById(R.id.recyclerViewCutomer);
    }

    private void getData(){
        DatabaseTest databaseTest = new DatabaseTest(this, MainActivity.databaseName,null,1);
        Cursor cursor = databaseTest.getData("SELECT * FROM " + MainActivity.tableCustomer);
        while (cursor.moveToNext()){
            String id = cursor.getString(0);
            String name = cursor.getString(1);
            String address = cursor.getString(2);
            String phone = cursor.getString(3);
            customerList.add(new Customer(id,name,address,phone));
        }
        customerListFilter.addAll(customerList);
    }
}