package com.hqsoft.esales.trainee;

import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hqsoft.esales.trainee.adapter.InventoryAdapter;
import com.hqsoft.esales.trainee.model.DatabaseTest;
import com.hqsoft.esales.trainee.model.Inventory;
import com.hqsoft.esales.trainee.my_interface.IOnChangeItemListener;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class DialogInvt extends DialogFragment {
    public interface IOrderListener{
        public void sendData(List<Inventory> list);
    }

    RecyclerView recyclerView;
    EditText editText;
    Button buttonClose,buttonConfirm;
    InventoryAdapter adapter;
    List<Inventory> inventoryList = new ArrayList<>();
    //Map<String,Inventory> map = new LinkedHashMap<>();
    List<Inventory> inventoryListFilter = new ArrayList<>();
    IOrderListener listener;

        @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.dialog_inventory,container,false);
        getDialog().setCanceledOnTouchOutside(false);

        recyclerView = view.findViewById(R.id.recyclerViewInvt);
        editText = view.findViewById(R.id.editText);
        buttonClose = view.findViewById(R.id.buttonClose);
        buttonConfirm = view.findViewById(R.id.buttonConfirm);

        getListInventory();

        LinearLayoutManager manager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(false);
        adapter = new InventoryAdapter(inventoryListFilter, new IOnChangeItemListener() {
            @Override
            public void OnChange(Inventory invt) {
//                if (map.containsKey(invt.getInvtID())){
//                    map.replace(invt.getInvtID(),invt);
//                }
//                else {
//                    map.put(invt.getInvtID(),invt);
//                }
                for (Inventory item : inventoryList){
                    if (item.getInvtID().compareTo(invt.getInvtID()) ==0){
                        item.setAmount(invt.getAmount());
                        break;
                    }
                }
            }
        });
        recyclerView.setAdapter(adapter);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                inventoryListFilter.clear();
                String filter = String.valueOf(s);
                if (filter.compareTo("") != 0){
                    for (Inventory item : inventoryList){
                        if (item.getInvtID().contains(filter) || item.getName().contains(filter)){
                            inventoryListFilter.add(item);
                        }
                    }
                }else{
                    inventoryListFilter.addAll(inventoryList);
                }
                adapter.notifyDataSetChanged();
            }
        });

        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Inventory> listOrder = new ArrayList<>();
                for (Inventory item : inventoryList){
                    if (item.getAmount() != 0) {
                        listOrder.add(item);
                    }
                }
                listener.sendData(listOrder);
                dismiss();
            }
        });

        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return view;
    }

    private void getListInventory(){
        DatabaseTest databaseTest = new DatabaseTest(getContext(), MainActivity.databaseName,null,1);
        Cursor cursor = databaseTest.getData("SELECT * FROM " + MainActivity.tableInventory);
        while (cursor.moveToNext()){
            String id = cursor.getString(0);
            String name = cursor.getString(1);
            String unit = cursor.getString(2);
            String price = cursor.getString(3);
            inventoryList.add(new Inventory(id,name,unit,price));
        }
        inventoryListFilter.addAll(inventoryList);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try{
            listener = (IOrderListener) getActivity();
        }catch (ClassCastException e){

        }
    }
}
