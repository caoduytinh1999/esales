package com.hqsoft.esales.trainee;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hqsoft.esales.trainee.model.DatabaseTest;
import com.hqsoft.esales.trainee.model.Session;

import java.io.File;
import java.sql.Timestamp;

public class MainActivity extends AppCompatActivity {

    public static String databaseName = "eSales1.db3";
    public static String tableSalesPerson = "AR_SALESPERSON";
    public static String tableCustomer = "AR_CUSTOMER";
    public static String tableInventory = "IN_INVENTORY";
    public static String tableSalesOrd = "OM_SALESORD";
    public static String tableSalesOrdDet = "OM_SALESORDDET";

    EditText editTextUser;
    EditText editTextPassword;
    Button buttonLogin;
    Button buttonExit;
    TextView textViewInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //createDatabase();
        mapped();
        event();
    }

    private void event() {

        textViewInfo.setText("Phiên Bản Ngày : " + getInfoInstall() + "\n" + "eSales Version 1.0");

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidate()){
                    String user = editTextUser.getText().toString().trim();
                    String password = editTextPassword.getText().toString().trim();
                    if (login(user,password)){
                        Toast.makeText(MainActivity.this, "Đăng nhập thành công", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(MainActivity.this,CustomerActivity.class));
                    }
                    else{
                        Toast.makeText(MainActivity.this, "Thông tin đăng nhập chưa đúng!Vui lòng kiểm tra lại", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishAffinity();
            }
        });
    }

    private void mapped() {
        editTextUser = findViewById(R.id.editTextUserName);
        editTextPassword = findViewById(R.id.editTextPassWord);
        buttonLogin = findViewById(R.id.buttonLogin);
        buttonExit = findViewById(R.id.buttonExit);
        textViewInfo = findViewById(R.id.textViewInfo);
    }

    private boolean checkValidate(){
        String username = editTextUser.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();

        if (username.compareTo("") == 0){
            editTextUser.setError("Vui lòng nhập thông tin username");
            editTextUser.setFocusable(true);
            return false;
        }
        else{
            editTextUser.setError(null);
        }

        if (password.compareTo("") ==0){
            editTextPassword.setError("Vui lòng thông tin password");
            editTextPassword.setFocusable(true);
            return false;
        }
        else{
            editTextPassword.setError(null);
        }

        return true;
    }

    private boolean login(String checkUser,String checkPassword){
        DatabaseTest databaseTest = new DatabaseTest(this, databaseName,null,1);
        Cursor cursor = databaseTest.getData("SELECT * FROM " + tableSalesPerson);
        while (cursor.moveToNext()){
            String user = cursor.getString(0);
            String password = cursor.getString(1);
            if (user.compareTo(checkUser) ==0 && password.compareTo(checkPassword) ==0){
                Session session = new Session(this);
                session.setSalesPerSon(user);
                return true;
            }
        }
        return false;
    }

    private String getInfoInstall(){
        String packageName = getApplicationContext().getPackageName();
        PackageManager pm = getPackageManager();
        ApplicationInfo appInfo = null;
        try {
            appInfo = pm.getApplicationInfo(packageName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String appFile = appInfo.sourceDir;
        long installed = new File(appFile).lastModified();
        String date = new Timestamp(installed).toString();
        String day = date.split(" ")[0].split("-")[2];
        String month = date.split(" ")[0].split("-")[1];
        String year = date.split(" ")[0].split("-")[0];
        String hour = date.split(" ")[1].split(":")[0];
        String minute = date.split(" ")[1].split(":")[1];

        return year + month + day + "_" + hour + minute;
    }

    public void createDatabase(){
        DatabaseTest databaseTest = new DatabaseTest(this, databaseName,null,1);

//        databaseTest.queryData("CREATE TABLE IF NOT EXISTS " + tableSalesPerson + "(SlsperID TEXT PRIMARY KEY, Password TEXT,FullName TEXT, Address TEXT)");
//        databaseTest.queryData("INSERT INTO " + tableSalesPerson + " VALUES('SlsperID1','sa1','Nguyễn Văn A','72/24 Phan Đăng Lưu, P5, Phú Nhuận, HCM')");
//        databaseTest.queryData("INSERT INTO " + tableSalesPerson + " VALUES('SlsperID2','sa2','Nguyễn Văn B','72/24 Phan Đăng Lưu, P5, Phú Nhuận, HCM')");
//        databaseTest.queryData("INSERT INTO " + tableSalesPerson + " VALUES('SlsperID3','sa3','Nguyễn Văn C','72/24 Phan Đăng Lưu, P5, Phú Nhuận, HCM')");
//        databaseTest.queryData("INSERT INTO " + tableSalesPerson + " VALUES('SlsperID4','sa4','Nguyễn Văn D','72/24 Phan Đăng Lưu, P5, Phú Nhuận, HCM')");
//        databaseTest.queryData("INSERT INTO " + tableSalesPerson + " VALUES('SlsperID5','sa5','Nguyễn Văn E','72/24 Phan Đăng Lưu, P5, Phú Nhuận, HCM')");

//        databaseTest.queryData("CREATE TABLE IF NOT EXISTS " + tableCustomer + "(CustID TEXT PRIMARY KEY, Name TEXT,Address TEXT, Phone TEXT)");
//        databaseTest.queryData("INSERT INTO " + tableCustomer + " VALUES('CustID1', 'Khách Hàng A', '72/24 Phan Đăng Lưu, P5, Phú Nhuận, HCM', '1234567890')");
//        databaseTest.queryData("INSERT INTO " + tableCustomer + " VALUES('CustID2', 'Khách Hàng B', '72/24 Phan Đăng Lưu, P5, Phú Nhuận, HCM', '1234567891')");
//        databaseTest.queryData("INSERT INTO " + tableCustomer + " VALUES('CustID3', 'Khách Hàng C', '72/24 Phan Đăng Lưu, P5, Phú Nhuận, HCM', '1234567892')");
//        databaseTest.queryData("INSERT INTO " + tableCustomer + " VALUES('CustID4', 'Khách Hàng D', '72/24 Phan Đăng Lưu, P5, Phú Nhuận, HCM', '1234567893')");
//        databaseTest.queryData("INSERT INTO " + tableCustomer + " VALUES('CustID5', 'Khách Hàng E', '72/24 Phan Đăng Lưu, P5, Phú Nhuận, HCM', '1234567894')");

//        databaseTest.queryData("CREATE TABLE IF NOT EXISTS " + tableInventory + "(InvtID TEXT PRIMARY KEY, Name TEXT,Unit TEXT, Price TEXT)");
//        databaseTest.queryData("INSERT INTO " + tableInventory + " VALUES('SP1', 'Sản Phẩm 1', 'CHAI', '10000')");
//        databaseTest.queryData("INSERT INTO " + tableInventory + " VALUES('SP2', 'Sản Phẩm 2', 'LON', '15000')");
//        databaseTest.queryData("INSERT INTO " + tableInventory + " VALUES('SP3', 'Sản Phẩm 3', 'CHAI', '20000')");
//        databaseTest.queryData("INSERT INTO " + tableInventory + " VALUES('SP4', 'Sản Phẩm 4', 'LON', '17000')");
//        databaseTest.queryData("INSERT INTO " + tableInventory + " VALUES('SP5', 'Sản Phẩm 5', 'LON', '32000')");

        //databaseTest.queryData("CREATE TABLE IF NOT EXISTS " + tableSalesOrd + "(OrderNbr TEXT PRIMARY KEY, SlsperID TEXT,CustID TEXT, OrdAmt REAL, OrdQty REAL, OrderDate DATETIME, Remark TEXT)");
        //databaseTest.queryData("INSERT INTO " + tableSalesOrd + " VALUES('Sales10001', 'Sales1', 'CustID1', '35000', '3', '2018-08-09', 'Ghi chú ')");
//        databaseTest.queryData("INSERT INTO " + tableSalesOrd + " VALUES('Sales10002', 'Sales1', 'CustID1', '50000', '5', '2018-08-09', 'Ghi chú ')");
//        databaseTest.queryData("INSERT INTO " + tableSalesOrd + " VALUES('Sales20001', 'Sales2', 'CustID2', '40000', '2', '2018-08-09', 'Ghi chú ')");
//        databaseTest.queryData("INSERT INTO " + tableSalesOrd + " VALUES('Sales30001', 'Sales3', 'CustID3', '51000', '3', '2018-08-09', 'Ghi chú ')");
//        databaseTest.queryData("INSERT INTO " + tableSalesOrd + " VALUES('Sales40001', 'Sales4', 'CustID4', '128000', '4', '2018-08-09', 'Ghi chú ')");

        //databaseTest.queryData("CREATE TABLE IF NOT EXISTS " + tableSalesOrdDet + "(OrderNbr TEXT, LineRef TEXT, InvtID TEXT, LineAmt REAL, LineQty REAL, PRIMARY KEY (OrderNbr, LineRef))");
//        databaseTest.queryData("INSERT INTO "+ tableSalesOrdDet + " VALUES('Sales10001', '00001', 'SP1', '20000', '2')");
//        databaseTest.queryData("INSERT INTO "+ tableSalesOrdDet + " VALUES('Sales10001', '00002', 'SP2', '15000', '1')");
//        databaseTest.queryData("INSERT INTO "+ tableSalesOrdDet + " VALUES('Sales10002', '00001', 'SP1', '50000', '5')");
//        databaseTest.queryData("INSERT INTO "+ tableSalesOrdDet + " VALUES('Sales30001', '00001', 'SP3', '40000', '2')");
//        databaseTest.queryData("INSERT INTO "+ tableSalesOrdDet + " VALUES('Sales20001', '00001', 'SP4', '51000', '3')");
//        databaseTest.queryData("INSERT INTO "+ tableSalesOrdDet + " VALUES('Sales40001', '00001', 'SP5', '128000', '4')");


        Cursor cursor = databaseTest.getData("SELECT * FROM "+ tableSalesOrd);
        while(cursor.moveToNext()){
            String name = cursor.getString(2);
            Log.d("AAA",name);
        }

    }
}