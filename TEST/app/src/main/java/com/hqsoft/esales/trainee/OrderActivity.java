package com.hqsoft.esales.trainee;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hqsoft.esales.trainee.adapter.OrderAdapter;
import com.hqsoft.esales.trainee.model.DatabaseTest;
import com.hqsoft.esales.trainee.model.Inventory;
import com.hqsoft.esales.trainee.model.Session;
import com.hqsoft.esales.trainee.my_interface.IOnClickItemListener;
import com.hqsoft.esales.trainee.my_interface.OnClick;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class OrderActivity extends AppCompatActivity implements DialogInvt.IOrderListener{
    Toolbar toolbar;
    TextView textViewSum;
    RecyclerView recyclerView;
    EditText editTextNote;
    Button buttonAddInvt,buttonConfirm;
    List<Inventory> inventoryList = new ArrayList<>();
    OrderAdapter adapter;
    double sum = 0;
    int qty = 0;
    String CustID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        mapped();
    }

    @Override
    protected void onResume() {
        super.onResume();
        event();
    }

    private void event() {

        Intent intent = getIntent();
        CustID = intent.getStringExtra("CustID");

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        LinearLayoutManager manager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        adapter = new OrderAdapter(inventoryList, new IOnClickItemListener() {
            @Override
            public void OnClick(OnClick onClick) {

            }
        });
        recyclerView.setAdapter(adapter);

        buttonAddInvt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });

        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (inventoryList.size() != 0){
                    confirmOrder();
                }
                else{
                    Toast.makeText(OrderActivity.this, "Vui lòng chọn sản phẩm", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void confirmOrder(){
        String note = editTextNote.getText().toString().trim();
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        String date = year + "-" +(month < 10 ? "0" + month : month) + "-" +(day < 10 ? "0" + day : day);
        String salesPerSon = new Session(this).getSalesPerson();
        UUID uuid = UUID.randomUUID();
        String orderNbr = "CustID_" + uuid.toString();
        DatabaseTest databaseTest = new DatabaseTest(this, MainActivity.databaseName,null,1);
        databaseTest.queryData("INSERT INTO '" + MainActivity.tableSalesOrd + "' VALUES('"+ orderNbr +"','"+ salesPerSon +"','"+ CustID +"','" + sum + "', '" + qty + "', '"+ date +"', '"+ note +"')");
        DecimalFormat format = new DecimalFormat("00000");
        int index = 1;
        for (Inventory item : inventoryList){
            String lineRef = format.format(index);
            index++;
            double lineAmt = Double.parseDouble(item.getPrice()) * item.getAmount();
            databaseTest.queryData("INSERT INTO '" + MainActivity.tableSalesOrdDet + "' VALUES('" + orderNbr + "','"+ lineRef +"','"+ item.getInvtID() +"','" + lineAmt +"','" + item.getAmount() +"')");
        }
        inventoryList.clear();
        adapter.notifyDataSetChanged();
        editTextNote.setText("");
        textViewSum.setText("");
        Toast.makeText(this, "Mua Hàng Thành công", Toast.LENGTH_SHORT).show();
    }

    private void mapped() {
        toolbar = findViewById(R.id.toolbar);
        textViewSum = findViewById(R.id.textViewSum);
        recyclerView = findViewById(R.id.recyclerView);
        editTextNote = findViewById(R.id.editTextNote);
        buttonAddInvt = findViewById(R.id.buttonAddInvt);
        buttonConfirm = findViewById(R.id.buttonConfirm);
    }

    private void showDialog(){
        DialogInvt dialog = new DialogInvt();
        dialog.show(getSupportFragmentManager(),"DialogInvt");
    }


//    @Override
//    public void sendData(Map<String, Inventory> map) {
//        if (inventoryList.size() == 0){
//            for (String id : map.keySet()){
//                inventoryList.add(map.get(id));
//                double price = Double.parseDouble(map.get(id).getPrice());
//                int amount = map.get(id).getAmount();
//                sum += price * amount;
//                qty += amount;
//            }
//        }
//        else{
//            for(String id : map.keySet()){
//                boolean check = true;
//                for (Inventory item : inventoryList){
//                    if (item.getInvtID().compareTo(id)==0){
//                        item.setAmount(item.getAmount() + map.get(id).getAmount());
//                        check = false;
//                    }
//                }
//                if (check){
//                    inventoryList.add(map.get(id));
//                }
//                double price = Double.parseDouble(map.get(id).getPrice());
//                int amount = map.get(id).getAmount();
//                sum += price * amount;
//                qty += amount;
//            }
//        }
//        adapter.notifyDataSetChanged();
//        DecimalFormat format = new DecimalFormat("###,###,###");
//        textViewSum.setText(format.format(sum));
//    }

    @Override
    public void sendData(List<Inventory> list) {
        if (inventoryList.size() ==0){
            for (Inventory item : list){
                inventoryList.add(item);
                double price = Double.parseDouble(item.getPrice());
                int amount = item.getAmount();
                sum += price * amount;
                qty += amount;
            }
        }
        else{
            for(Inventory newItem : list){
                boolean check = true;
                for (Inventory item : inventoryList){
                    if (item.getInvtID().compareTo(newItem.getInvtID())==0){
                        item.setAmount(item.getAmount() + newItem.getAmount());
                        check = false;
                    }
                }
                if (check){
                    inventoryList.add(newItem);
                }
                double price = Double.parseDouble(newItem.getPrice());
                int amount = newItem.getAmount();
                sum += price * amount;
                qty += amount;
            }
        }
        adapter.notifyDataSetChanged();
        DecimalFormat format = new DecimalFormat("###,###,###");
        textViewSum.setText(format.format(sum));
    }
}