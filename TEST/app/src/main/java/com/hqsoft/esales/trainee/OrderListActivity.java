package com.hqsoft.esales.trainee;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.hqsoft.esales.trainee.adapter.OrderListAdapter;
import com.hqsoft.esales.trainee.model.DatabaseTest;
import com.hqsoft.esales.trainee.model.SalesOrd;
import com.hqsoft.esales.trainee.model.Session;
import com.hqsoft.esales.trainee.my_interface.IOnClickItemListener;
import com.hqsoft.esales.trainee.my_interface.OnClick;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class OrderListActivity extends AppCompatActivity {

    Toolbar toolbar;
    ImageView imageView;
    EditText editText;
    RecyclerView recyclerView;
    TextView textView;
    List<SalesOrd> salesOrdList = new ArrayList<>();
    List<SalesOrd> salesOrdList_Filter = new ArrayList<>();
    String dateFilter = "";
    OrderListAdapter adapter;
    String CustID = "";
    double sum = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        mapped();
       // event();
    }

    @Override
    protected void onResume() {
        super.onResume();
        event();
    }

    private void event() {
        Intent intent = getIntent();
        CustID = intent.getStringExtra("CustID");

        if (CustID == null){
            CustID = new Session(this).getCustomer();
        }
        else{
            new Session(this).setCustomer(CustID);
        }

        getCurrentDate();
        getData();
        filterOrder();

        LinearLayoutManager manager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        adapter = new OrderListAdapter(salesOrdList_Filter, new IOnClickItemListener() {
            @Override
            public void OnClick(OnClick onClick) {

            }
        });
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(OrderListActivity.this,OrderActivity.class);
                intent1.putExtra("CustID",CustID);
                startActivity(intent1);
            }
        });
    }

    private void getCurrentDate(){
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        editText.setText((day < 10 ? "0" + day : day) + "/" + (month < 10 ? "0" + month : month) + "/" + year);
        dateFilter = year + "-" + (month < 10 ? "0" + month : month) + "-" + (day < 10 ? "0" + day : day);
    }

    private void mapped() {
        toolbar = findViewById(R.id.toolbarOrder);
        imageView = findViewById(R.id.imageViewAdd);
        editText = findViewById(R.id.editTextSearch);
        recyclerView = findViewById(R.id.recyclerViewOrder);
        textView = findViewById(R.id.textViewSum);
    }

    private void getData(){
        DatabaseTest databaseTest = new DatabaseTest(this, MainActivity.databaseName,null,1);
        Cursor cursor = databaseTest.getData("SELECT * FROM '" + MainActivity.tableSalesOrd + "' WHERE CustID = '" + CustID +"'");
        while (cursor.moveToNext()){
            String OrderNbr = cursor.getString(0);
            String SlsperID = cursor.getString(1);
            String CustID = cursor.getString(2);
            double OrdAmt = cursor.getDouble(3);
            double OrdQty = cursor.getDouble(4);
            String OrderDate = cursor.getString(5);
            String Remark = cursor.getString(6);
            sum += OrdAmt;
            salesOrdList.add(new SalesOrd(OrderNbr,SlsperID,CustID,OrdAmt,OrdQty,OrderDate,Remark));
        }
        DecimalFormat format = new DecimalFormat("###,###,###");
        textView.setText("Tổng : " + format.format(sum));
    }

    public void showDatePicker(){
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month++;
                String date = (dayOfMonth < 10 ? "0" + dayOfMonth : dayOfMonth) + "/" + (month < 10 ? "0" + month : month) + "/" + year;
                editText.setText(date);
                dateFilter = year + "-" + (month < 10 ? "0" + month : month) + "-" + (dayOfMonth < 10 ? "0" + dayOfMonth : dayOfMonth);
                filterOrder();
                adapter.notifyDataSetChanged();
            }

        }, year, month, day);
        dialog.getDatePicker().setMaxDate(new Date().getTime());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void filterOrder(){
        salesOrdList_Filter.clear();
        sum = 0;
        for (SalesOrd item : salesOrdList){
            if (dateFilter.compareTo(item.getOrderDate()) ==0){
                salesOrdList_Filter.add(item);
                sum += item.getOrdAmt();
            }
        }
        DecimalFormat format = new DecimalFormat("###,###,###");
        textView.setText("Tổng : " + format.format(sum));
    }
}