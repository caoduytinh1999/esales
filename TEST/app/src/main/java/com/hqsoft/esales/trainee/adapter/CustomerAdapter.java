package com.hqsoft.esales.trainee.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.hqsoft.esales.trainee.model.Customer;
import com.hqsoft.esales.trainee.R;
import com.hqsoft.esales.trainee.my_interface.IOnClickItemListener;

import java.util.List;

public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.ViewHoler>{
    List<Customer> list;
    IOnClickItemListener listener;

    public CustomerAdapter(List<Customer> list, IOnClickItemListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @Override
    public ViewHoler onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_customer,parent,false);
        return new ViewHoler(view);
    }

    @Override
    public void onBindViewHolder(CustomerAdapter.ViewHoler holder, int position) {
        Customer customer = list.get(position);
        holder.textViewNumber.setText(String.valueOf(position += 1));
        String id = customer.getCustID();
        String name = customer.getName();
        String phone = customer.getPhone();
        holder.textViewCutomer.setText(id + " - " + name + '\n' + "ĐT : " + phone);
        holder.textViewAddress.setText(customer.getAddress());
        if (position % 2 != 0){
            holder.linearLayout.setBackground((ContextCompat.getDrawable(holder.linearLayout.getContext(),R.color.white)));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHoler extends RecyclerView.ViewHolder{
        TextView textViewNumber,textViewCutomer,textViewAddress;
        LinearLayout linearLayout;
        public ViewHoler(View itemView) {
            super(itemView);
            textViewNumber = itemView.findViewById(R.id.textViewNumber);
            textViewCutomer = itemView.findViewById(R.id.textViewCustomer);
            textViewAddress = itemView.findViewById(R.id.textViewAddress);
            linearLayout = itemView.findViewById(R.id.layoutRowCustom);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.OnClick(list.get(getAdapterPosition()));
                }
            });
        }
    }
}
