package com.hqsoft.esales.trainee.adapter;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.hqsoft.esales.trainee.model.Inventory;
import com.hqsoft.esales.trainee.R;
import com.hqsoft.esales.trainee.my_interface.IOnChangeItemListener;

import java.text.DecimalFormat;
import java.util.List;

public class InventoryAdapter extends RecyclerView.Adapter<InventoryAdapter.ViewHolder>{
    List<Inventory> list;
    IOnChangeItemListener listener;

    public InventoryAdapter(List<Inventory> list, IOnChangeItemListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_inventory,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InventoryAdapter.ViewHolder holder, int position) {
        Inventory inventory = list.get(position);
        int number = position + 1;
        holder.textViewNumber.setText(String.valueOf(number));
        String id = inventory.getInvtID();
        String name = inventory.getName();
        holder.textViewInvt.setText(id + " - " + name);
        DecimalFormat format = new DecimalFormat("###,###,###");
        holder.textViewPrice.setText(format.format(Double.parseDouble(inventory.getPrice())));
        holder.editTextQty.setText(String.valueOf(inventory.getAmount()));
        if (position % 2 != 0){
            holder.linearLayout.setBackground((ContextCompat.getDrawable(holder.linearLayout.getContext(),R.color.white)));
        }

        holder.editTextQty.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus){
                    String amount = holder.editTextQty.toString();
                    if (amount.compareTo("") == 0){
                        holder.editTextQty.setText("0");
                        inventory.setAmount(Integer.parseInt(holder.editTextQty.getText().toString()));
                    }
                }
            }
        });

        holder.editTextQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String amount = String.valueOf(s);
                if (amount.compareTo("0") != 0 && amount.compareTo("") != 0){
                    list.get(position).setAmount(Integer.parseInt(holder.editTextQty.getText().toString()));
                    listener.OnChange(list.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        LinearLayout linearLayout;
        TextView textViewNumber,textViewInvt,textViewPrice;
        EditText editTextQty;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            linearLayout = itemView.findViewById(R.id.layoutInvt);
            textViewNumber = itemView.findViewById(R.id.textViewNumber);
            textViewInvt = itemView.findViewById(R.id.textViewInvt);
            textViewPrice = itemView.findViewById(R.id.textViewPrice);
            editTextQty = itemView.findViewById(R.id.editTextQty);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    }
}
