package com.hqsoft.esales.trainee.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.hqsoft.esales.trainee.model.Inventory;
import com.hqsoft.esales.trainee.R;
import com.hqsoft.esales.trainee.my_interface.IOnClickItemListener;

import java.text.DecimalFormat;
import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder>{
    List<Inventory> list;
    IOnClickItemListener listener;

    public OrderAdapter(List<Inventory> list, IOnClickItemListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_order,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderAdapter.ViewHolder holder, int position) {
        Inventory inventory = list.get(position);
        holder.textViewNumber.setText(String.valueOf(position += 1));
        holder.textViewID.setText(inventory.getInvtID());
        holder.textViewName.setText(inventory.getName());
        DecimalFormat format = new DecimalFormat("###,###,###");
        holder.textViewPrice.setText(format.format(Double.parseDouble(inventory.getPrice())));
        holder.textViewQty.setText(inventory.getAmount() + "");
        double price = Double.parseDouble(inventory.getPrice());
        int amount = inventory.getAmount();
        holder.textViewSum.setText(format.format(price * amount));
        if (position % 2 != 0){
            holder.linearLayout.setBackground((ContextCompat.getDrawable(holder.linearLayout.getContext(),R.color.white)));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        LinearLayout linearLayout;
        TextView textViewNumber,textViewID,textViewName,textViewPrice,textViewQty,textViewSum;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            linearLayout = itemView.findViewById(R.id.layoutOrder);
            textViewNumber = itemView.findViewById(R.id.textViewNumber);
            textViewID = itemView.findViewById(R.id.textViewIDInvt);
            textViewName = itemView.findViewById(R.id.textViewNameInvt);
            textViewPrice = itemView.findViewById(R.id.textViewPrice);
            textViewQty = itemView.findViewById(R.id.textViewQty);
            textViewSum = itemView.findViewById(R.id.textViewSum);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

        }
    }
}
