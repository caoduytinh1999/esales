package com.hqsoft.esales.trainee.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.hqsoft.esales.trainee.R;
import com.hqsoft.esales.trainee.model.SalesOrd;
import com.hqsoft.esales.trainee.my_interface.IOnClickItemListener;

import java.text.DecimalFormat;
import java.util.List;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.ViewHolder>{
    List<SalesOrd> list;
    IOnClickItemListener listener;

    public OrderListAdapter(List<SalesOrd> list, IOnClickItemListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_order_list,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderListAdapter.ViewHolder holder, int position) {
        SalesOrd salesOrd = list.get(position);
        holder.textViewNumber.setText(String.valueOf(position += 1));
        holder.textViewOrderNumber.setText(salesOrd.getOrderNbr());
        DecimalFormat format = new DecimalFormat("###,###,###");
        holder.textView.setText(format.format(salesOrd.getOrdAmt()) + "");
        if (position % 2 != 0){
            holder.linearLayout.setBackground((ContextCompat.getDrawable(holder.linearLayout.getContext(),R.color.white)));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        LinearLayout linearLayout;
        TextView textViewNumber,textViewOrderNumber,textView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            linearLayout = itemView.findViewById(R.id.layoutRowOrder);
            textViewNumber = itemView.findViewById(R.id.textViewNumber);
            textViewOrderNumber = itemView.findViewById(R.id.textViewOrderNumber);
            textView = itemView.findViewById(R.id.textView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.OnClick(list.get(getAdapterPosition()));
                }
            });
        }
    }
}
