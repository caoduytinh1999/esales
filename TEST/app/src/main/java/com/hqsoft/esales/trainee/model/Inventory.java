package com.hqsoft.esales.trainee.model;

import com.hqsoft.esales.trainee.my_interface.OnClick;

public class Inventory extends OnClick {
    private String invtID;
    private String name;
    private String unit;
    private String price;
    private int amount;

    public Inventory(String invtID, String name, String unit, String price) {
        this.invtID = invtID;
        this.name = name;
        this.unit = unit;
        this.price = price;
        this.amount = 0;
    }

    public Inventory(String invtID, String name, String unit, String price, int amount) {
        this.invtID = invtID;
        this.name = name;
        this.unit = unit;
        this.price = price;
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getInvtID() {
        return invtID;
    }

    public void setInvtID(String invtID) {
        this.invtID = invtID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
