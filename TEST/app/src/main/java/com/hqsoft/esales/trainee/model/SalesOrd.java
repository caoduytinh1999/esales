package com.hqsoft.esales.trainee.model;

import com.hqsoft.esales.trainee.my_interface.OnClick;

public class SalesOrd extends OnClick {
    private String orderNbr;
    private String sisperID;
    private String custID;
    private double ordAmt;
    private double ordQty;
    private String orderDate;
    private String remark;

    public SalesOrd(String orderNbr, String sisperID, String custID, double ordAmt, double ordQty, String orderDate, String remark) {
        this.orderNbr = orderNbr;
        this.sisperID = sisperID;
        this.custID = custID;
        this.ordAmt = ordAmt;
        this.ordQty = ordQty;
        this.orderDate = orderDate;
        this.remark = remark;
    }

    public String getOrderNbr() {
        return orderNbr;
    }

    public void setOrderNbr(String orderNbr) {
        this.orderNbr = orderNbr;
    }

    public String getSisperID() {
        return sisperID;
    }

    public void setSisperID(String sisperID) {
        this.sisperID = sisperID;
    }

    public String getCustID() {
        return custID;
    }

    public void setCustID(String custID) {
        this.custID = custID;
    }

    public double getOrdAmt() {
        return ordAmt;
    }

    public void setOrdAmt(double ordAmt) {
        this.ordAmt = ordAmt;
    }

    public double getOrdQty() {
        return ordQty;
    }

    public void setOrdQty(double ordQty) {
        this.ordQty = ordQty;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
