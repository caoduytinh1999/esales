package com.hqsoft.esales.trainee.model;

public class SalesPerson {
    private String slsperID;
    private String passWord;
    private String fullName;
    private String address;

    public SalesPerson(String slsperID, String passWord, String fullName, String address) {
        this.slsperID = slsperID;
        this.passWord = passWord;
        this.fullName = fullName;
        this.address = address;
    }

    public String getSlsperID() {
        return slsperID;
    }

    public void setSlsperID(String slsperID) {
        this.slsperID = slsperID;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
