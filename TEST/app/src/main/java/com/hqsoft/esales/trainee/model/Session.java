package com.hqsoft.esales.trainee.model;

import android.content.Context;
import android.content.SharedPreferences;

public class Session {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    public Session(Context mContext){
        preferences = mContext.getSharedPreferences("AppKey",mContext.MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
    }

    public void setSalesPerSon(String username){
        editor.putString("user",username);
        editor.apply();
    }

    public String getSalesPerson(){
        return preferences.getString("user","");
    }

    public void setCustomer(String customer){
        editor.putString("customer",customer);
        editor.apply();
    }

    public String getCustomer(){
        return preferences.getString("customer","");
    }

}
