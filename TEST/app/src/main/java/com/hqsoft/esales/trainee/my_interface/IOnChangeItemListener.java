package com.hqsoft.esales.trainee.my_interface;

import com.hqsoft.esales.trainee.model.Inventory;

public interface IOnChangeItemListener {
    public void OnChange(Inventory invt);
}
